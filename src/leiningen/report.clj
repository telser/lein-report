(ns leiningen.report
  (:require [leiningen.core.main :as main]
            [leiningen.core.project :as project]
            [leiningen.eastwood :as e]
            [report.document :as doc]
            [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.java.shell :as sh]))

(def report-help
  (str
    "lein report: Generate a report of your project based on several lein plugins.\n"
    "Plugins for report generation are currently:
          ancient
          eastwood (work in progress)
          dep-graph
          kibit

  For all options please see the online documentation at:

  https://gitlab.com/telser/lein-report"))

;; Unfortunately since different plugins can, and do, handle output in different
;; ways, support for each plugin has to be added to capture the result of each.
(defn- ancient-support
  "Wrapping ancient..."
  [project args outfile flag]
  (let [args (if (some #(= ":no-colors" %) args)
               args
               (conj args ":no-colors"))
        result (with-out-str (main/apply-task (main/lookup-alias "ancient" project)
                                              project args))
        item-start "\\begin{itemize}\n"
        item-end "\\end{itemize}\n"
        item "\\item"
        tex-line-result (str/join " \n" (map #(str item %1)
                                             (str/split (str/replace
                                                         result #"\[|\]" " ")
                                                        #"\n")))]
    (println args)
    (case flag
      "subsub" (if (empty? tex-line-result)
                 (.write outfile
                         (doc/subsubsection "ancient" "No results were given"))
                 (.write outfile
                         (doc/subsubsection "ancient" (str item-start
                                                           tex-line-result
                                                           item-end))))
      "sub" (if (empty? tex-line-result)
                 (.write outfile
                         (doc/subsection "ancient" "No results were given"))
                 (.write outfile
                         (doc/subsection "ancient" (str item-start
                                                        tex-line-result
                                                        item-end))))
      (println "Something terrible happened processing the ancient subtask"
               "The result as given by ancient is: " result))))

;; TODO: Fix the image size so that they will fit
;; Better yet, allow a config option for image size.
(defn- dep-graph-support
  "Wrapping dep-graph..."
  [project args outfile flag]
  (let [result (with-out-str (main/apply-task (main/lookup-alias "dep-graph"
                                                                 project)
                                              project args))]
    (case flag
      "subsub" (do (.write outfile
                           (doc/subsubsection
                            "dep-graph"
                            (str "\\includegraphics[width=11cm, height=12cm]"
                                 "{" (:name project) ".eps}")))
                   (sh/sh "dot" "-Teps" (str "-o" (:name project) ".eps")
                          (str (:name project) ".dot"))
                   (sh/sh "rm" (str (:name project) ".dot")))
      "sub" (do (.write outfile
                           (doc/subsection
                            "dep-graph"
                            (str "\\includegraphics[width=11cm, height=12cm]"
                                 "{" (:name project) ".eps}")))
                   (sh/sh "dot" "-Teps" (str "-o" (:name project) ".eps")
                          (str (:name project) ".dot"))
                   (sh/sh "rm" (str (:name project) ".dot")))
      (println "Something terrible happened processing the dep-graph subtask."
               "The result as given by dep-graph is: " result))))

;; TODO: Does this need the tex-line-result like ancient?
(defn- kibit-support
  "Wrapping kibit"
  [project args outfile flag]
  (let [result (with-out-str (main/apply-task (main/lookup-alias "kibit" project)
                                              project args))]
    (case flag
      "subsub" (if (empty? result)
                 (.write outfile
                         (doc/subsubsection "kibit" "No results were given"))
                 (.write outfile
                         (doc/subsubsection "kibit" result)))
      "sub" (if (empty? result)
                 (.write outfile
                         (doc/subsection "kibit" "No results were given"))
                 (.write outfile
                         (doc/subsection "kibit" result)))
      (println "Something terrible happened processing the kibit subtask"
               "The result as given by kibit is: " result))))

(defn- plugin-section
  "Write the plugin specific sections. A flag is set for section type.
   This has to be called in the contex of with-open!"
  [project plugin args outfile flag]
  (let [result (with-out-str (main/apply-task (main/lookup-alias plugin project)
                                              project args))]
    (case plugin
      "ancient" (ancient-support project args outfile flag)
      "dep-graph" (dep-graph-support project args outfile flag)
      "eastwood" (println "Eastwood support is a work in progress! Sorry. :(")
      "kibit" (kibit-support project args outfile flag)
      (println "You tried to use an unsupported plugin!"
               "Please see the list of supported plugins and the roadmap"
               "for additional support at gitlab.com/telser/lein-report"))))

(defn build-subproject-report
  "Build a report for a project with subprojects"
  [project outfile subs plugins plugin-opts & args]
  (let [sec (doc/section (:name project) (:description project))]
    (with-open [file (io/writer outfile)]
      (do
        (.write file (str (doc/preamble (str (:name project) " Report"))
                          "\n" sec "\n"))
        (doseq [each subs]
          (let [prj (project/init-project
                     (project/read (str each "/project.clj")))]
            (.write file (doc/subsection (:name prj) (:description prj)))
            (doseq [p plugins]
              (let [opts ((keyword p) plugin-opts)
                    opts (if opts
                           (map str opts)
                           [])]
                (plugin-section prj p opts file "subsub")))))
        (.write file (str doc/postamble))))))


;; TODO: This needs to be better. Detect which plugins are used and act accordingly.
;; Ex. dep-graph will output .eps files which should be deleted as intermediates
(defn compile-report-pdf
  "Run pdflatex on the tex file (twice) to generate a pdf then remove generated files"
  [outfile keep-intermediates?]
  (do
    (sh/sh "pdflatex" outfile)
    (sh/sh "pdflatex" outfile)
    (if keep-intermediates?
      ()
      (sh/sh "rm" "*.toc" " *.aux" "*-eps-converted*" "*.eps"))))

(defn build-project-report
  "Build a project-level report, no subprojects"
  [project outfile plugins plugin-opts & args]
  (let [sec (doc/section (:name project) (:description project))]
    (with-open [file (io/writer outfile)]
      (do
        (.write file (str (doc/preamble (str (:name project) " Report"))
                          "\n" sec "\n"))
        (doseq [p plugins]
          (let [opts ((keyword p) plugin-opts)
                opts (if opts
                       opts
                       [])]
            (plugin-section project p opts file "sub")))))))

(defn report
  ([project & args]
     (if (= args "help")
       (println report-help)
       (let [subs (seq (:sub project))
             opts (:report project)
             outfile (if (:outfile opts)
                       (:outfile opts)
                       (str (:name project) "-report.tex"))
             plugins (:plugins opts)
             plugin-opts (:plugin-opts opts)
             pdf? (:pdf opts)
             report
             (if subs ; Run in subtask mode
               (build-subproject-report project outfile subs plugins plugin-opts args)
               (build-project-report project outfile plugins plugin-opts args))]
         (if pdf?
           (do
             (compile-report-pdf outfile false)
             (println "The report was printed to a pdf. The file is " outfile))
           (println "The report was generated as a LaTex file at " outfile))))))
