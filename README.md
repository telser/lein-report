# lein-report

A Leiningen plugin to generate a report about a project based on information given from various other Leiningen plugins.

## What does this thing do?

In short, this will generate a LaTex report about your project. The information is fed by other Leiningen plugins, such as Ancient, which can tell you what dependencies are out of date, and Kibit which is a static analysis tool. Lein-report makes the collecting and compliation of this information automatable.

## Usage

Put `[lein-report "0.1.1"]` into the `:plugins` vector of your project.clj.

In order for this plugin to do anything at all, there are a few dependencies.
Any of the supported plugins that you plan to use must be installed.
Further, for dep-graph support you'll need dot installed and on the PATH.
Finally, you'll need LaTex installed and pdflatex available on the PATH. The only LaTex packages currently used by the template are graphicx and hyperref.

Currently supported plugins:

    * Ancient
    * dep-graph
    * Kibit

Since lein-report is wrapping around other Leiningen plugins and command line options could become very confusing very quickly, or at best cumbersome, lein-report accepts ** NO ** commandline arguments. So it is run as follows:

    $ lein report

**Some configuration is necessary for this to be useful!** See below for more information.

### How To Specify Plugins Used And Pass Args

Since lein-report relies on the output of other plugins, it is necessary to select which of the supported plugins are used for a given report. Naturally passing arguments to the plugins used for generating the report is also desirable. Handling of both of these is done in the project.clj file of your project. Have a section called :report with a subsection called :plugins. This should contain a vector of the plugins you wish to run. Something like:

    :report {
        :plugins ["ancient" "dep-graph" "kibit"]
        ...

**Do note that the order here is the order in which they will appear in the report!**

Options passed to these plugins are also configured under the report section of your project.clj and can be given like this:

    :report {
        :plugins ["ancient"]
        :plugin-opts {:ancient [":all" ":no-colors"]}
        ...

**NOTE: the no-colors option is actually required for the ancient plugin at this time so that the output doesn't get garbled.**

### Changing The File Saved

By default lein-report will save to $PROJECT-NAME-report.tex. However this behavior can be overridden by using the following:

    :report {
        :outfile "your-filename-here.tex"

## Bugs, Issues, Plugin Support

Please file bugs, report issues and see the plugin support roadmap at:

    gitlab.com/telser/lein-report

## License

Copyright © 2014 Trevis Elser

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
